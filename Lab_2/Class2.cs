﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab_2;

namespace lab_2
{
    public class ParentClass
    {
        public int publicVar;
        private int privateVar;
        protected int protectedVar;
        internal int internalVar;
        private protected int privateProtectedVar;
        internal protected int internalProtectedVar;
    }
    public class ChildClass: ParentClass
    {
        public void function()
        {
            publicVar = 0;
           // privateVar = 0;
            protectedVar = 0;
            internalVar = 0;
            privateProtectedVar = 0;
            internalProtectedVar = 0;
        }
    }
}
