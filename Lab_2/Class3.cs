﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2
{
    class Parent
    {
        int value;
        void func() { }
        class InsertClass
        {
            int value;
        }
    }

    class Child : Parent
    {
        public void childF()
        {
            /*value = 0;
            func();
            InsertClass.value = 0;*/
        }
    }
    struct MyStruct
    {
        int var;
        void method() { }
    }
    interface IMyInterface
    {
        void func();
    }
    class Realization : IMyInterface
    {
        public void func()
        {
            Console.WriteLine("Hello");
        }
    }
}
