﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab_2;



namespace lab_2
{
    

    internal class Program
    {
        static void outFunction(int number, out int result)
        {
            result = number * 2;
        }
        static void refFunction(ref int number)
        {
            number *= 2;
        }

        static void Main(string[] args)
        {
            MyObject obj = new MyObject('a', 'b');

            Console.WriteLine(obj.ToString());
            Console.WriteLine("Хеш-код об'єкта: " + obj.GetHashCode());

            MyObject obj2 = new MyObject('a', 'c');
            Console.WriteLine("Об'єкти рівні: " + obj.Equals(obj2));
        }

    }
}




