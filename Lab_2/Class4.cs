﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2
{
    public class MyClass
    {
        protected class ProtectedClas
        {
            public void func() { }
        }
        private class PrivateClas
        {
            public void func() { }
        }
        public class InternalClas
        {
            public void func() { }
        }
        ProtectedClas pt = new ProtectedClas();
        PrivateClas pv = new PrivateClas();
        InternalClas it = new InternalClas();
    }
}
