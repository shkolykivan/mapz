﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2
{
    interface IShape
    {
        double GetArea();
    }
    abstract class AbstractShape
    {
        public abstract double GetPerimeter();
    }
    class Rectangle : AbstractShape, IShape
    {
        public double Width { get; set; }
        public double Height { get; set; }

        public Rectangle(double width, double height)
        {
            Width = width;
            Height = height;
        }

        public override double GetPerimeter()
        {
            return 2 * (Width + Height);
        }
        public double GetArea()
        {
            return Width * Height;
        }
    }
}
