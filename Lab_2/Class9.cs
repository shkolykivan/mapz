﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab3
{
    public class NumberClass
    {
        public float value;

        public NumberClass(float value)
        {
            this.value = value;
        }

        public static explicit operator int(NumberClass a)
        {
            return (int)a.value;
        }

        public static implicit operator double(NumberClass a)
        {
            return (double)a.value;
        }

        /*static void Main(string[] args)
        {
            NumberClass number = new NumberClass(6.4f);
            Console.WriteLine($"Float: {number.value}");
            Console.WriteLine($"Int: {(int)number}");
            double doubleValue = number;
            Console.WriteLine($"Double: {doubleValue}");
        }*/
    }
}
