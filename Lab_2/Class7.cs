﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2
{
    class Device
    {
        protected string brand;
        protected double price;
        public Device(string brand, double price)
        {
            this.brand = brand;
            this.price = price;
        }
        public Device(string brand)
        {
            this.brand = brand;
            price = 0;
        }
        public virtual void DisplayInfo()
        {
            Console.WriteLine($"Brand: {brand}, price: {price}");
        }
    }
    class Phone : Device
    {
        private string model;
        public Phone(string model, string brand, double price) : base(brand, price)
        {
            this.model = model;
        }
        public Phone(string model, string brand) : this(model, brand, 0) { }
        public override void DisplayInfo()
        {
            Console.WriteLine($"Model: {model}, brand: {brand}, price: {price}");
        }
    }
}
