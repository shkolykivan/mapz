﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2
{
    class StaticAndDynamic
    {
        public static int staticValue;
        public int dynamicValue;
        static StaticAndDynamic()
        {
            staticValue = 1;
            Console.WriteLine("Static constructor created");
        }
        public StaticAndDynamic(int dynamicValue)
        {
            this.dynamicValue = dynamicValue;
            Console.WriteLine("Dynamic constructor created");
        }
        public StaticAndDynamic()
        {
            Console.WriteLine($"Instance created: {dynamicValue}");
        }

        public int getStatic()
        {
            return staticValue;
        }
    }
}
