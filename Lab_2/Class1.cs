﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2
{
    public interface IFigure
    {
        void Draw();
    }

    public abstract class Shape : IFigure
    {
        public string Color { get; set; }
        public double Size { get; set; }

        public Shape(string color, double size)
        {
            Color = color;
            Size = size;
        }

        public abstract void Draw();
    }

    public class Circle : Shape
    {
        public double Radius { get; set; }
        public Circle(string color, double size, double radius) : base(color, size)
        {
            Radius = radius;
        }

        public override void Draw()
        {
            Console.WriteLine($"Drawing a {Color} circle with radius {Radius} and size {Size}");
        }
    }

}
