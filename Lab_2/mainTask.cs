﻿using System.Diagnostics;
using System;

public class Program1
{
    public class ConstructorInitiliaze
    {
        private int x;
        private int y;
        public ConstructorInitiliaze(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public class NonConstructorInitiliaze
    {
        private int x = 10;
        private int y = 20;
    }
    public class StaticInitialization
    {
        private static int x = 10;
        private static int y = 20;
    }

    public class StaticConstructorInitialization
    {
        private static int x;
        private static int y;
        public StaticConstructorInitialization() { x = 10; y = 20; }
    }

    /*public static void Main(string[] args)
    {
        int iterationsCount = 100000000;
        Stopwatch stopwatch = new Stopwatch();
        // Ініціалізація змінних у конструкторі
        stopwatch.Start();
        for (int i = 0; i < iterationsCount; i++)
        {
            ConstructorInitiliaze obj = new ConstructorInitiliaze(1, 2);
        }
        stopwatch.Stop();
        Console.WriteLine("Час створення об'єктiв типу ConstructorInitiliase: {0} мс", stopwatch.ElapsedMilliseconds);

        // Ініціалізації змінних поза конструктором
        stopwatch.Reset();
        stopwatch.Start();
        for (int i = 0; i < iterationsCount; i++)
        {
            NonConstructorInitiliaze obj = new NonConstructorInitiliaze();
        }
        stopwatch.Stop();
        Console.WriteLine("Час створення об'єктiв типу NonConstructorInitiliase: {0} мс", stopwatch.ElapsedMilliseconds);

        // Статична ініціалізація змінних 
        stopwatch.Reset();
        stopwatch.Start();
        for (int i = 0; i < iterationsCount; i++)
        {
            StaticInitialization obj = new StaticInitialization();
        }
        stopwatch.Stop();
        Console.WriteLine("Час створення об'єктiв типу StaticInitialization: {0} мс", stopwatch.ElapsedMilliseconds);

        // Ініціалізація змінних у статичному конструкторі
        stopwatch.Reset();
        stopwatch.Start();
        for (int i = 0; i < iterationsCount; i++)
        {
            StaticConstructorInitialization obj = new StaticConstructorInitialization();
        }
        stopwatch.Stop();
        Console.WriteLine("Час створення об'єктiв типу StaticConstructorInitialization: {0} мс", stopwatch.ElapsedMilliseconds);
    }*/
}
