﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_2
{
    public class MyObject : Object
    {
        public int x;
        public int y;
        public MyObject(char x, char y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return $"It is new obgect{x}, {y}";
        }
        public override int GetHashCode()
        {
            return x.GetHashCode() & y.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is MyObject;
        }
    }
}
